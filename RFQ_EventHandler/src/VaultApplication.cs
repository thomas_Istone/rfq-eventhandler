﻿using MFiles.VAF;
using MFiles.VAF.Common;
using MFilesAPI;
using System;

namespace RFQ_EventHandler
{




    public class VaultApplication : VaultApplicationBase
    {
        /// <summary>
        /// Simple configuration member. MFConfiguration-attribute will cause this member to be loaded from the named value storage from the given namespace and key.
        /// Here we override the default alias of the Configuration class with a default of the config member.
        /// The default will be written in the named value storage, if the value is missing.
        /// Use Named Value Manager to change the configurations in the named value storage.
        /// </summary>

        //[MFClass(Required = false)]
        //public MFIdentifier Class_RFQ = "CL.RFQ";
        //[MFPropertyDef(Required = true)]
        //public MFIdentifier Property_RFQ = "PD.Project";

        [MFConfiguration("RFQ_Eventhandler", "config")]
        private readonly Configuration config = new Configuration() { Class_RFQ = "CL.RFQ", PropertyRQAlias = "PD.Project" };
        /// <summary>
        /// Eventhandler for permission changes
        /// </summary>
        /// 

        [EventHandler(MFEventHandlerType.MFEventHandlerAfterCheckInChangesFinalize, ObjectType = 0)]
        public void HandlePermissionsOnDocumentCheckIn(EventHandlerEnvironment env)
        {
            //var RFQProperty = env.ObjVerEx.Properties.GetProperty(config.PropertyRQAlias);
            PropertyValue RFQProperty;// = new PropertyValue();
            env.ObjVerEx.Properties.TryGetProperty(config.PropertyRQAlias, out RFQProperty);

            if (RFQProperty != null)
            {
                Lookup rfqObject = RFQProperty.Value.GetValueAsLookup();
                if (rfqObject != null)
                {
                    var foundObjVerEx = new ObjVerEx(env.Vault, rfqObject.ObjectType, rfqObject.Item, rfqObject.Version);
                    env.Vault.ObjectOperations.ChangePermissionsToACL(env.ObjVer, foundObjVerEx.ACL, false);
                }
            }

        }
        [EventHandler(MFEventHandlerType.MFEventHandlerAfterSetObjectPermissions)] //Configuration.ClassRFQAlias)]
        public void HandlePermissionChange(EventHandlerEnvironment env)
        {
            try
            {
                if (env.ObjVerEx.Class == config.Class_RFQ)
                {

                    var permissions = env.ObjVerEx.Permissions;

                    var relatedDocs = FindConnectedDocuments(config.PropertyRQAlias, env.ObjVer, env.Vault);
                    SetPermissionsofMultipleObjects(env.ObjVerEx.Permissions.AccessControlList, relatedDocs, env.Vault);
                }
            }
            catch (Exception ex)
            {
                SysUtils.ReportErrorToEventLog(ex);
            }


        }
        private void SetPermissionsofMultipleObjects(AccessControlList accList, ObjVers objectsToUpdate, Vault vault)
        {

            foreach (ObjVer relatedDoc in objectsToUpdate)
            {
                vault.ObjectOperations.ChangePermissionsToACL(relatedDoc, accList, false);
            }
        }
        private ObjVers FindConnectedDocuments(object propertyId, ObjVer relatedObjectVer, Vault vault)
        {
            var searchBuilder = new MFSearchBuilder(vault);
            PropertyDef propertyRfq = vault.PropertyDefOperations.GetPropertyDef(config.PropertyRQAlias);
            searchBuilder.References(config.PropertyRQAlias, relatedObjectVer);
            var searchResults = searchBuilder.Find();
            if (searchResults.Count > 0)
            {
                return searchResults.GetAsObjectVersions().GetAsObjVers();
            }
            return null;
        }

    }
}