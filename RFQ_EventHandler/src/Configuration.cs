﻿using MFiles.VAF.Common;
namespace RFQ_EventHandler
{
    /// <summary>
    /// Configuration of aliases used.
    /// </summary>
    public class Configuration
    {
        /// <summary>
        /// Reference to a test class.
        /// </summary>
        /// 
        internal const string ClassRFQAlias = "CL.RFQ";
        internal const string PropertyRFQALias = "PD.Project";

        [MFClass(Required = false)]
        public MFIdentifier Class_RFQ = Configuration.ClassRFQAlias;
        [MFObjType(Required = false)]
        public MFIdentifier ObjectType_Document = Configuration.ClassRFQAlias;

        [MFPropertyDef(Required = true)]
        public MFIdentifier PropertyRQAlias = Configuration.PropertyRFQALias;

    }
}
